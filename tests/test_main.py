"""
This file test the main function
"""
import os
import tempfile
import json
import yaml
import python_repository_example.main as pre

CURRENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
FIXTURES_FOLDER = os.path.join(CURRENT_FOLDER, "fixtures")


def test_convert_file_json_yaml():
    """
    This function test convert file json yaml
    """
    input_file = os.path.join(FIXTURES_FOLDER, "test_file.json")
    output_file = os.path.join(tempfile.gettempdir(), "test_file.yaml")
    # Get converter
    output_path = pre.convert_file(
        input_file=input_file,
        input_format="json",
        output_file=output_file,
        output_format="yaml",
    )
    # Load yaml file
    with open(output_path, "r") as output_file:
        raw_data = yaml.load(output_file.read(), Loader=yaml.FullLoader)
        result = raw_data[0]["John"]["name"]
    expected_result = "John Doe"
    assert result == expected_result


def test_convert_file_yaml_json():
    """
    This function test convert file json yaml
    """
    input_file = os.path.join(FIXTURES_FOLDER, "test_file.yaml")
    output_file = os.path.join(tempfile.gettempdir(), "test_file.json")
    # Get converter
    output_path = pre.convert_file(
        input_file=input_file,
        input_format="yaml",
        output_file=output_file,
        output_format="json",
    )
    # Load yaml file
    with open(output_path, "r") as output_file:
        raw_data = json.loads(output_file.read())
        result = raw_data[0]["John"]["name"]
    expected_result = "John Doe"
    assert result == expected_result


def test_Main_empty_content():
    """
    This function test convert file json yaml
    """
    # Get converter
    m = pre.Main(
        input_file="",
        input_format="json",
        output_file="",
        output_format="yaml",
    )
    try:
        m.write_file()
    except RuntimeError, error:
        result = str(error)
    expected_result = "Without content we can't write anything."
    assert result == expected_result


def test_convert_from_lang():
    """
    This function test an unsupported lang for the convert_from
    """
    try:
        pre.convert_from(content=[], lang="toto")
    except RuntimeError, error:
        result = str(error)
    expected_result = "This language is not supported.\n\
Please use a language in this list : {}".format(
    ", ".join(pre.SUPPORTED_FORMAT))
    assert result == expected_result


def test_convert_to_lang():
    """
    This function test an unsupported lang for the convert_from
    """
    try:
        pre.convert_to(content=[], lang="toto")
    except RuntimeError, error:
        result = str(error)
    expected_result = "This language is not supported.\n\
Please use a language in this list : {}".format(
    ", ".join(pre.SUPPORTED_FORMAT))
    assert result == expected_result


def test_convert_from_None():
    """
    This function test when a user override SUPPORTED_FORMAT
    but it's not included in the code
    """
    # Override SUPPORTED_FORMAT
    pre.SUPPORTED_FORMAT.append("toto")
    result = pre.convert_from(content=[], lang="toto")
    expected_result = None
    assert result == expected_result


def test_convert_to_None():
    """
    This function test when a user override SUPPORTED_FORMAT
    but it's not included in the code
    """
    # Override SUPPORTED_FORMAT
    pre.SUPPORTED_FORMAT.append("toto")
    result = pre.convert_to(content=[], lang="toto")
    expected_result = None
    assert result == expected_result
