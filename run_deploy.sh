#!/usr/bin/env bash
# ---
# Clean Previous Run
clear

# ---
# Pimp Env
ORIG_PYTHONPATH=${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:${PWD}
echo "| PYTHONPATH : ${PYTHONPATH}"

# Launch Deploy

# ---
# Restore Env for Local Testing
export PYTHONPATH=${ORIG_PYTHONPATH}