#!/usr/bin/env bash
# ---
# Clean Previous Run
clear
find ./ -type d -name "public" -exec rm -rf "{}" \;
find ./ -name "gl-code-quality-report.json" -delete

# ---
# Pimp Env
ORIG_PYTHONPATH=${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:${PWD}
echo "| PYTHONPATH : ${PYTHONPATH}"

# ---
# Run pylint
echo "| Create Dir : public/quality"
mkdir -p public/quality
echo "| Launch Pylint"
pylint -j 4 --rcfile=.pylintrc ./python_repository_example | tee public/quality/pylint_report_python_full.txt
SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' public/quality/pylint_report_python_full.txt)
echo "| Pylint Score : $SCORE"

# ---
# Generate Badge
echo "| Generate Badge"
if [[ $SCORE == *"-"* ]]; then
  SCORE="--${SCORE}"
fi
SUBJECT="pylint"
BADGE_URL="https://img.shields.io/badge/${SUBJECT}-${SCORE}-brightgreen.svg"
curl ${BADGE_URL} -o public/quality/pylint_badge.svg
echo "  | Badge : public/quality/pylint_badge.svg"

# ---
# Gen Report
echo "| Generate GitLab Report"
python pylint_report_converter -i public/quality/pylint_report_python_full.txt -o gl-code-quality-report.json -f gitlab

# ---
# Restore Env for Local Testing
export PYTHONPATH=${ORIG_PYTHONPATH}