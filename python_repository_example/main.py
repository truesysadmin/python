"""
This Module contain the main Function and Class for convert a file from
json, yaml to json, yaml.
Look at the ``SUPPORTED_FORMAT`` var for supported format.
"""
import logging
import json
import yaml

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

SUPPORTED_FORMAT = ["json", "yaml"]


def convert_from(content, lang):
    """
    This function convert the content from a language to Python.

    :param str content: the content in the specific language.
    :param str lang: the language.

    :rtype: Python
    :return: The content in Python.
    """
    if lang not in SUPPORTED_FORMAT:
        raise RuntimeError(
            "This language is not supported.\n\
Please use a language in this list : {}".format(
                ", ".join(SUPPORTED_FORMAT)
            )
        )

    if lang == "json":
        return json.loads(content)
    elif lang == "yaml":
        return yaml.load(content, Loader=yaml.FullLoader)

    return None


def convert_to(content, lang):
    """
    This function convert the content in Python to the specific language.

    :param str content: the content in the specific language.
    :param str lang: the language language.

    :rtype: Specified language
    :return: The content in the str specific language
    """
    if lang not in SUPPORTED_FORMAT:
        raise RuntimeError(
            "This language is not supported.\n\
Please use a language in this list : {}".format(
                ", ".join(SUPPORTED_FORMAT)
            )
        )

    if lang == "json":
        return json.dumps(content)
    elif lang == "yaml":
        return yaml.dump(content)

    return None


class Main(object):
    """
    This Class Main manage the conversion of a file from json, yaml to json, yaml.
    """

    def __init__(self, input_file, input_format, output_file, output_format):
        """
        Init the class. Use the the ``self.all_in_one()`` to process.

        :param str input_file: The input file path.
        :param str input_format: The input file format. Look at ``python_repository_example.main.SUPPORTED_FORMAT``
        :param str output_file: The output file path.
        :param str output_format: The output file format. Look at ``python_repository_example.main.SUPPORTED_FORMAT``

        """
        self.input_file = input_file
        self.input_format = input_format
        self.output_file = output_file
        self.output_format = output_format

        self.content = {}

    def all_in_one(self):
        """
        This function is the all in one process, it read the file and write
        the file into the required file format.

        :rtype: str
        :return: return the output file path
        """
        # read file
        self.read_file()

        # write output file
        path = self.write_file()

        return path

    def read_file(self):
        """
        This function read the file, and translate it in Python.

        :rtype: python
        :return: The content of the file in Python.
        """
        with open(self.input_file, "r") as input_file:
            LOGGER.info("Read File : {}".format(self.input_file))
            # Read the content and convert it
            raw_content = input_file.read()
            self.content = convert_from(
                content=raw_content, lang=self.input_format
            )
        return self.content

    def write_file(self):
        """
        This function encode in the required format and write the file.

        :rtype: str
        :return: The output file path.
        """
        if not self.content:
            raise RuntimeError("Without content we can't write anything.")
        # Convert Python to Output format
        output_content = convert_to(
            content=self.content, lang=self.output_format
        )
        # Write the file
        with open(self.output_file, "w") as output_file:
            LOGGER.info("Write File : {}".format(self.output_file))
            output_file.write(output_content)
        return self.output_file


def convert_file(input_file, input_format, output_file, output_format):
    """
    This function convert a file from a format to another by using the Main Class

    :param str input_file: the file path
    :param str input_format: the input file format
    :param str output_file: the file path
    :param str output_format: the output file format

    :rtype: str
    :return: The output file path
    """
    # Instancing the class
    m = Main(
        input_file=input_file,
        input_format=input_format,
        output_file=output_file,
        output_format=output_format,
    )
    path = m.all_in_one()

    return path
