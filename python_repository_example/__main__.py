"""
CLI file for python_repository_example

The ``python_repository_example`` module is also available in CLI.
You can use it by launching the command below.

.. code-block:: bash

    python_repository_example [-h] -i INPUT_FILE -if INPUT_FORMAT -o OUTPUT_FILE -of OUTPUT_FORMAT

Below, the help provided by the ``python_repository_example -h`` command.

.. code-block:: bash
    :linenos:

    usage: python_repository_example [-h] -i INPUT_FILE -if INPUT_FORMAT -o
                                    OUTPUT_FILE -of OUTPUT_FORMAT

    This converter, convert from a format to another. The supported format are :
    json, yaml

    optional arguments:
    -h, --help            show this help message and exit
    -i INPUT_FILE, --input_file INPUT_FILE
                            The Input File Path.
    -if INPUT_FORMAT, --input_format INPUT_FORMAT
                            The Input Format.
    -o OUTPUT_FILE, --output_file OUTPUT_FILE
                            The Output File Path.
    -of OUTPUT_FORMAT, --output_format OUTPUT_FORMAT
                            The Output format.

"""
import sys
import os
import argparse
import python_repository_example.main as pre


if __name__ == "__main__":
    ARGV = sys.argv[1:]
    # Get the arguments
    PARSER = argparse.ArgumentParser(
        description="This converter, convert from a format to another.\n\
The supported format are : {}".format(
            ", ".join(pre.SUPPORTED_FORMAT)
        ),
        epilog="",
    )
    PARSER.add_argument(
        "-i", "--input_file", required=True, help="The Input File Path."
    )
    PARSER.add_argument(
        "-if", "--input_format", required=True, help="The Input Format."
    )
    PARSER.add_argument(
        "-o", "--output_file", required=True, help="The Output File Path."
    )
    PARSER.add_argument(
        "-of", "--output_format", required=True, help="The Output format."
    )
    ARGS = PARSER.parse_args(ARGV)

    RESULT = pre.convert_file(
        input_file=os.path.abspath(ARGS.input_file),
        input_format=ARGS.input_format,
        output_file=os.path.abspath(ARGS.output_file),
        output_format=ARGS.output_format,
    )

    exit(RESULT)
