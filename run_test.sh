#!/usr/bin/env bash
# ---
# Clean Previous Run
clear
find ./ -type d -name "public" -exec rm -rf "{}" \;
find ./ -name ".pytest_cache" -exec rm -rf "{}" \;
find ./ -name "*.pyc" -delete
find ./ -name ".coverage" -delete
find ./ -name "report.xml" -delete

# ---
# Pimp Env
ORIG_PYTHONPATH=${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:${PWD}
echo "| PYTHONPATH : ${PYTHONPATH}"

# ---
# Run test
echo "| Create Dir : public/test"
mkdir -p public/test
echo "| Launch pytest"
py.test -v --cache-clear --color=yes --cov-report html --cov=./python_repository_example --junitxml=report.xml tests/
echo "| Move html > public/test/"
mv htmlcov public/test/
coverage report -m

# ---
# Restore Env for Local Testing
export PYTHONPATH=${ORIG_PYTHONPATH}
