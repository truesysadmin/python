Python Repository Example
======================================

This Python Respository Example show you,
how you can structure your Python repository.

    - The First part, concern a Classic Code Documentation for Python Code.
    - The Second part, explain How to Setup your Repository. It's more like a tutorial or a check-list.

Classic Documentation
----------------------
.. toctree::
    :maxdepth: 2

    user_guide
    api_reference
    reports


How To Setup Your Repository
-----------------------------

.. toctree::
    :maxdepth: 2

    how_to/index


Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
