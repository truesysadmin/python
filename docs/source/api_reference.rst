API Reference
######################

Main Module
==================================================

.. automodule:: python_repository_example.main
    :members:
    :private-members:
    :show-inheritance:

CLI
==================================================

.. automodule:: python_repository_example.__main__
    :members:
    :private-members:
    :inherited-members:
    :show-inheritance:
