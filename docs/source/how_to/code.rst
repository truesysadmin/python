Code
######

Coding Convention
==============================
Follow the Coding Convention defined in the ``README.md`` file.
If you've correctly setuped your IDE, it should follow the convention definded
in the ``.pylintrc`` file and highlight each wrong line.

Code Formatter - Black
------------------------

Black is a Code Formatter, if you prefer to code instead of loose your time
to align, indent, change the quote, etc...

Switch to `Black <https://github.com/python/black>`_.
He's doing the job for you.

Formatting Off
+++++++++++++++

  If you don't want to format a part of your code insert ``# fmt: off`` before and ``# fmt: on`` after.
  Example :

  .. code-block:: python

    # fmt: off
    super_list = ['too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long', 'too-long']
    # fmt: on


Setup VSCode
++++++++++++++++
For the VSCode lovers look at this `link <https://code.visualstudio.com/docs/python/editing#_formatting>`_.
My VScode settings for Black as examlple.

.. code-block:: python

  "python.formatting.blackPath": "/Users/mdubrois/bin/black.sh",
  "editor.formatOnSave": true,

black.sh
+++++++++++++
For your information the content of my ``black.sh``

.. code-block:: bash

    #!/bin/bash
    export PYTHONPATH=/usr/local/lib/python3.7/site-packages

    # Launch Black
    /Library/Frameworks/Python.framework/Versions/3.7/bin/black --line-length 79 "$@"

.. note::

    Don't forget to chmod 775 your ``black.sh``
