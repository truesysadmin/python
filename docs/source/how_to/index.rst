How To Setup Your Repository
##############################

.. toctree::
    :maxdepth: 2

    init_files
    code
    docs
    cicd
    deploy
