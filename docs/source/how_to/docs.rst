Documentation
#################

Define the ProjectName and Version
==================================================
In Sphinx you must define the Project Name, Version and Copyright.

If you use the ``conf.py`` from this repository,
you can easily define this vars at the top of the file.

.. code-block:: python

    import python_repository_example
    # Get Project Name and Version from Git
    PROJECT_NAME = "Python Repository Example"
    VERSION = python_repository_example.__version__
    COPYRIGHT = "2019, Marc Dubrois"


Define your Icon
==================================================
    - Simply put an ico in the `_static` folder like `_static/icon.ico`.
    - And set this path in your ``conf.py`` file like below.

.. code-block:: python

    html_favicon = "_static/icon.ico"


Convert a PNG to Icon
++++++++++++++++++++++++++++++++++++++++++++++++++
You can easily convert a PNG file to an ICO file by using the online converter `zamzar <https://www.zamzar.com>`_


Document also the __init__
==================================================
Found on `developer.ridgerun.com <https://developer.ridgerun.com/wiki/index.php/How_to_generate_sphinx_documentation_for_python_code_running_in_an_embedded_system#Editing_conf.py>`_.
That's allow to autodoc also the ``__init__`` function.

.. code-block:: python

    # Ensure that the __init__ method gets documented.
    def skip(app, what, name, obj, skip, options):
        if name == "__init__":
            return False
        return skip


    def setup(app):
        app.connect("autodoc-skip-member", skip)
