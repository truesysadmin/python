Init Files
##############

.gitignore
==================================================
To init the ``.gitignore``, copy/paste the ``.gitignore`` file from
this repository or create a new file from GitLab.

Create .gitignore from GitLab
--------------------------------------------------
    - In the repository near to the `branch` menu.
    - Click on **+** and then select `New File`.
    - Select `.gitignore`
    - And pick-up the template `Python`.

Like me, you should consider to add the following lines at the end of the Files

.. code-block:: bash

    # ----
    # Added by User
    .DS_Store
    .metadata
    .vscode

.gitlab-ci.yml
==================================================
The ``.gitlab-ci.yml`` allow you to define yours jobs for the CICD.
Copy/Paste the file from this repository.
We will explain later how to custom it.

.pylintrc
==================================================
The ``.pylintrc`` is the settings file for `Pylint <https://www.pylint.org/>`_.
Pylint is a code analyzer for Python, it ensure you respect the convention
defined in your ``.pylintrc`` settings.

Install Pylint
--------------------------------------------------
Install Pylint by following these `instructions <https://www.pylint.org/#install>`_.

Init .pylintrc
--------------------------------------------------
To init the ``.pylintrc`` file, you can Copy/Paste the file from this repository
or simply generate a new one by using the command below.

.. code-block:: bash

    pylint --generate-rcfile > ./.pylintrc


python_folder
==================================================
Create a folder with the name of your repository
and the ``__init__.py`` associated.

.. note::

    Replace each '-' by a '_' in your folder name.
