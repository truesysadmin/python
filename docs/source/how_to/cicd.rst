GitLab CICD
###############

This page describe how to setup your `CICD <https://docs.gitlab.com/ee/ci/introduction/index.html#basic-cicd-workflow>`_ jobs for GitLab.

Stages & Jobs
================
Usually, we want several Stages before deploying our code.
In Python, we don't need a `Build` Stage. So our CICD could be :

.. mermaid::

    graph LR;
        Q("Check the Code Quality");
        T("Test the Code");
        D("Generate the Documentation");
        Y("Deploy");
        Q-->T;
        T-->D;
        D-->Y;

        style Q fill:#FFE51E
        style T fill:#6FA2FF
        style D fill:#D076FF
        style Y fill:#20FF58

Usually, i define the Stages and Jobs like below.

.. mermaid::

    graph LR
        quality_full("Quality Full")
        quality_error("Quality Error")
        unit_test("Unit Test")
        build_doc("Build Documentation")
        pages("Pages")
        deploy("Deploy")

        subgraph Quality
        quality_full
        quality_error
        end

        quality_full --> unit_test
        quality_error --> unit_test

        subgraph Test
        unit_test
        end

        unit_test --> build_doc

        subgraph Doc
        build_doc
        end

        build_doc --> pages
        build_doc --> deploy

        subgraph Deploy
        pages
        deploy
        end

        style quality_full fill:#FFE51E
        style quality_error fill:#FFE51E
        style unit_test fill:#6FA2FF
        style build_doc fill:#D076FF
        style deploy fill:#20FF58
        style pages fill:#20FF58

.. figure::  ../_static/how_to/pipelines_cicd.png
    :align:   center

    Pipeline page on GitLab.

Jobs Detail
============
A brief description for each Job.

quality_full:
    - Test the quality of the code with pylint in full mode.
    - That's ensure every body respect the convention.
    - ``allow_failure: true`` / I don't want to failed for a warning.
    - But that's generate a pylint Badge with your score. If you have a bad Score shame on you.
    - That's also generate a Report for GitLab which is inserted in the Merge Request.

quality_error:
    - Test the quality of the code with pylint in Error mode.
    - ``allow_failure: false`` / In case of Error, the Job failed and you can't deploy.

unit_test:
    - That's test your code to ensure everything work fine.
    - That's generate a Code Coverage Report. Look at `Reports <../reports.html>`_
    - ``allow_failure: false``
    - That's also generate a report (junit) which is inserted in the Merge Request.

build_doc:
    - Simply build the doc with Sphinx.
    - Add Theme and Plugin like mermaid
    - ``allow_failure: false``

pages:
    - This Job pick every artifcats in `dependencies`_.
    - And publish it on `GitLab Pages <https://about.gitlab.com/product/pages/>`_.
    - Yours artifcats **must be** in the ``public`` folder.
    - ``allow_failure: false``

deploy:
    - Blah

Define Your CICD
===================
Everything is defined in the ``.gitlab-ci.yml`` file.

It's pretty simple, you simply have to declare in `yaml` the Stages and Jobs.
Look at the explanations below and to the `.gitlab-ci.yml <https://gitlab.com/md-shared/python-repository-example/blob/master/.gitlab-ci.yml>`_ of this repository.

Define yours Stages
----------------------
.. code-block:: yaml

    stages:
        - quality
        - build
        - test
        - doc
        - deploy

Define a Job
-----------------
.. code-block:: yaml

    # the job name.
    quality_job:
        # Defines the Stages.
        stage: quality
        # Defines which Docker image we want to use.
        image: centos:centos7.5.1804
        # If the job doesn't work, should we start the other jobs in dependency.
        allow_failure: true
        # A list of commands line to execute.
        script:
            - pip install -U pylint
            # You can also launch a bash file.
            # TIPS That's allow you to run locally the quality job.
            - ./run_quality.sh
        # Artifacts, allow to attached files to a job.
        # This files can be viewed on GitLab and called by another job.
        artifacts:
            paths:
                - public/quality/pylint_badge.svg
                - public/quality/pylint_report_python_full.txt
                - gl-code-quality-report.json
            # reports allow to be automatically ingested in your merge request for example
            reports:
                codequality: gl-code-quality-report.json

TIPS
----------------------
The full GitLab Documentation for ``.gitlab-ci.yml`` is available `GitLab Doc ".gitlab-ci.yml" <https://docs.gitlab.com/ee/ci/yaml/>`_.

before_script
++++++++++++++++++++++
Sometimes you want to execute the same code on each job.
And because we are lazy, we don't want to copy/paste code into every Job.
You can use the ``before_script`` key to do this.
Each line will be executed for each job.

.. code-block:: yaml

    before_script:
        - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
        - python get-pip.py
        - export PYTHONPATH=${PYTHONPATH}:$PWD
        - pip install PyYAML

tags
++++++++++++++++++++++
Add the key `tags` allow you to filter the runner.

.. code-block:: yaml

    tags:
        - mac
        - 128Go


coverage
++++++++++++++++++++++
Insert this key ``coverage`` in your **unit_test** job,
with a regular expression as value.
GitLab parse your log and extract your code coverage.
This Code Coverage appear in your Merge Request.

.. code-block:: yaml

    unit_test:
        coverage: '/TOTAL.*\s+(\d+%)$/'

dependencies
++++++++++++++++++++++
This key say "I need to grab the artifacts of this jobs for running the Job !".
It usefull, in case of publish reports or documentation built in previous job.
And also if you build a c++ code and then you want to test it.

.. code-block:: yaml

    dependencies:
        - quality_full
        - unit_test
        - build_doc

only
++++++++++++++++++++++
The ``only`` key is very important. That's allow you to choice on which event you want to run the Job.
`GitLab Doc "only" <https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic>`_.
You can run the job on Merge Request, Tag, Specific Branch, etc...

.. code-block:: yaml

    only:
        - master


Setup yours Badges in GitLab
===============================
.. figure:: ../_static/how_to/badges/pipeline.svg
.. figure:: ../_static/how_to/badges/coverage.svg
.. figure:: https://md-shared.gitlab.io/python-repository-example/quality/pylint_badge.svg

First the `Badge Documentation <https://docs.gitlab.com/ee/user/project/badges.html>`_.

Setup your Badge at the Group or Project Level
-----------------------------------------------
As described in the Doc, you can setup your GitLab repository to show
yours Badges directly on the Home page.

    - Go at the Group Level or Project Level
    - Settings > General and Click on Badges

Now define yours Badge Link and Badge path.

Built-In GitLab Badge
+++++++++++++++++++++++++
GitLab provide the Badges `build, pipeline, coverage`.

**At Group Level**

pipeline:
    - .. figure::  ../_static/how_to/badges/pipeline.svg
    - Link : ``https://gitlab.com/%{project_path}/pipelines``
    - Badge : ``https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg``

coverage:
    - .. figure::  ../_static/how_to/badges/coverage.svg
    - Link : ``https://gitlab.com/%{project_path}``
    - Badge : ``https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg``

**At Project Level**

At the Project level you can define more precisely a link for the coverage Bagde.
And point directly to the Coverage Report.

coverage:
    - .. figure::  ../_static/how_to/badges/coverage.svg
    - Link : ``https://md-shared.gitlab.io/python-repository-example/test/htmlcov/``
    - Badge : ``https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg``

Create Your Badge
+++++++++++++++++++++++++
Like in the ``run_quality.sh`` file, you can create your own Badge, like below.

.. figure:: https://img.shields.io/badge/My%20Awesome%20Super%20Badge-100%25-brightgreen.svg
