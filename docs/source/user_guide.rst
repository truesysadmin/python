User Guide
=============
You will find below the detail of the command line to use to convert Pylint messages into Code-Climate messages for GitLab.
As well as an example of a gitlab-ci file to know how to use it.

How to use it
----------------

The ``python_repository_example`` tool is available in command line.

To use it you must:
    - Add the ``pylint-report-converter`` folder to your ``PYTHONPATH``
    - Launch the command line

    .. code-block:: bash

        python python_repository_example -i INPUT_FILE -o OUTPUT_FILE [-f {gitlab}]

    - For example

    .. code-block:: bash

        python ./python_repository_example -i ./pylint_report_python_full.txt -o ./gl-code-quality-report.json -f gitlab

Below, the help provided by the ``python_repository_example -h`` command.

.. code-block:: bash
    :linenos:

    usage: python_repository_example [-h] -i INPUT_FILE -o OUTPUT_FILE [-f {gitlab}]

    optional arguments:
    -h, --help            show this help message and exit
    -i INPUT_FILE, --input_file INPUT_FILE
                            The Pylint input file path.
    -o OUTPUT_FILE, --output_file OUTPUT_FILE
                            The output file path.
    -f {gitlab}, --format {gitlab}
                            The output format.



How to Setup your GitLab CI with Pylint-Report-Converter
---------------------------------------------------------
You will find below how to use the converter with gitlab-ci.

In your ``.gitlab-ci.yml`` define a job like below for call and use ``pylint-report-converter``

.. code-block:: yaml

    my_pylint_job:
        stage: quality
        image: centos:centos7.5.1804
        allow_failure: true
        script:
            # Get Pip
            - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
            - python get-pip.py
            # Get Pylint-report-converter
            - git clone https://gitlab.com/mdubrois/pylint-report-converter.git /tmp/pylint-report-converter
            # Add Pylint-report-converter to the PYTHONPATH
            - export PYTHONPATH=${PYTHONPATH}:"/tmp/pylint-report-converter"
            # Add your repo to the PYTHONPATH
            - export PYTHONPATH=${PYTHONPATH}:$PWD
            # Install pylint
            - pip install -U pylint
            # Launch Pylint and catch the output to the file pylint_report.txt
            pylint -j 4 --output-format=parseable --rcfile=.pylintrc **/*.py | tee pylint_report.txt
            # Run Pylint-report-Converter
            - python /tmp/pylint-report-converter/python_repository_example -i ./pylint_report.txt -o ./gl-code-quality-report.json -f gitlab
        artifacts:
            paths:
                - gl-code-quality-report.json
            reports:
            codequality: gl-code-quality-report.json
